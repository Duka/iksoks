import socket
import threading
import json
import os
from queue import Queue

bufferSize = 1024

playerStats = []
matchQueue = Queue()
matches = {}
loggedInUsers = {}

def getLocalIP():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('8.8.8.8', 80))
        localIp = s.getsockname()[0]
    finally:
        s.close()
    return localIp

def startServer():
    localIP = getLocalIP()
    serverAddressPort = (localIP, 8080)
    serverSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serverSocket.bind(serverAddressPort)
    serverSocket.listen()
    print(f'Listening on {serverAddressPort}')
    
    global playerStats
    playerStats = loadPlayerStats()

    while True:
        conn, addr = serverSocket.accept()
        clientThread = threading.Thread(target=handleClient, args=(conn, addr))
        clientThread.start()

def handleClient(conn, addr):
    print(f'Connected: {addr}')
    while True:
        try:
            data = conn.recv(1024)
            if not data:
                print(f'Client {addr} disconnected')
                break

            print(f'Received from {addr}: {data.decode()}')

            try:
                request = json.loads(data.decode())
                if isinstance(request, dict) and "action" in request:
                    if request.get("action") == "login":
                        response = logInPlayer(request.get("username"), request.get("password"), conn)
                        print("Response from logInPlayer:", response)
                    elif request.get("action") == "register":
                        response = registerPlayer(request.get("username"), request.get("password"))
                    elif request.get("action") == "stats":
                        response = fetchPlayerStats(request.get("username"))
                    elif request.get("action") == "find_match":
                        response = findMatch(request.get("username"))
                        while True:
                            if request.get("username") in matchQueue.queue:
                                print("I am listening!")
                                data = conn.recv(1024)
                                if not data:
                                    print(f'Client {addr} disconnected')
                                    break
                                request = json.loads(data.decode())
                                if request.get("action") == "break":
                                    break
                                if request.get("action") == "leave_queue":
                                    print(f'Player {request.get("username")} leaving queue!')
                                    leaveQueue(request.get("username"))
                                    break
                    else:
                        response = json.dumps({"status": "failure", "message": "Invalid action"})
                else:
                    response = json.dumps({"status": "failure", "message": "Invalid request"})
            except json.JSONDecodeError:
                response = json.dumps({"status": "failure", "message": "Invalid JSON format"})
            conn.sendall(response.encode())
        except Exception as e:
            print(f'Error handling client {addr}: {e}')
            break

    print(f'I am no longer listening to: {addr}')
    conn.close()

def loadPlayerStats():
    scriptDir = os.path.dirname(__file__)
    filePath = os.path.join(scriptDir, 'playerStats.json')

    if not os.path.isfile(filePath):
        print(f"File {filePath} does not exist.")
        return []
    else:
        print(f"File {filePath} found.")
        with open(filePath, 'r') as file:
            print('Loaded player stats.')
            return json.load(file)

def dumpPlayerStats():
    scriptDir = os.path.dirname(__file__)
    filePath = os.path.join(scriptDir, 'playerStats.json')
    if not os.path.isfile(filePath):
        print(f"File {filePath} does not exist.")
    else:
        print(f"File {filePath} found.")
        with open('playerStats.json', 'w') as file:
            json.dump(playerStats, file, indent=4)
            print('Updated player stats file.')
    
    with open(filePath, 'w') as file:
        json.dump(playerStats, file, indent=4)
    print('Player stats saved.')

def logInPlayer(username, password, conn):
    global playerStats, loggedInUsers
    for user in playerStats:
        if user['name'] == username and user['password'] == password:
            loggedInUsers[username] = conn
            print(f"User {username} logged in")
            return json.dumps({"status": "success", "message": f"Welcome, {username}!"})
    return json.dumps({"status": "failure", "message": "Invalid username or password"})

def fetchPlayerStats(username):
    global playerStats, loggedInUsers
    for user in playerStats:
            if user['name'] == username:
                print(f"{username}'s stats fetched!")
                return json.dumps({"status": "success", "win": user["win"], "tie": user["tie"], "loss": user["loss"]})
    return json.dumps({"status": "failure", "message": "Failed to fetch stats"})

def registerPlayer(username, password):
    global playerStats
    for user in playerStats:
        if user['name'] == username:
            return json.dumps({"status": "failure", "message": "Username already exists"})

    newUser = {
        "name": username,
        "password": password,
        "win": 0,
        "tie": 0,
        "loss": 0
    }

    playerStats.append(newUser)
    dumpPlayerStats()
    return json.dumps({"status": "success", "message": f"User {username} registered successfully"})

def findMatch(username):
    global matchQueue, loggedInUsers
    if username not in loggedInUsers:
        return json.dumps({"status": "failure", "message": "User not logged in"})

    matchQueue.put(username)
    if matchQueue.qsize() >= 2:
        player1 = matchQueue.get()
        player2 = matchQueue.get()

        leaveQueue(player1)
        leaveQueue(player2)
        
        conn1 = loggedInUsers[player1]
        conn2 = loggedInUsers[player2]
        matchID = len(matches) + 1
        matches[matchID] = (player1, player2)
        print(f"Starting match {matchID} between {player1} and {player2}")
        
        board = [" "] * 9
        currentPlayer = player1
        secondPlayer = player2

        conn1.sendall(json.dumps({"status": "success", "message": "Match found!", "board": board, "currentPlayer": currentPlayer, "secondPlayer" : secondPlayer}).encode())
        conn2.sendall(json.dumps({"status": "success", "message": "Match found!", "board": board, "currentPlayer": currentPlayer, "secondPlayer" : secondPlayer}).encode())
        
        matchThread = threading.Thread(target=handleMatch, args=(conn1, conn2, player1, player2))
        matchThread.start()
        return json.dumps({"status": "success", "message": "Match found!", "matchID": matchID, "currentPlayer": currentPlayer, "secondPlayer" : secondPlayer})
    return json.dumps({"status": "waiting", "message": "Waiting for a match..."})

def leaveQueue(username):
    global matchQueue
    if username in matchQueue.queue:
        matchQueue.queue.remove(username)
        print(f"{username} removed from match queue")
        return json.dumps({"status": "success", "message": f"{username} removed from match queue"})
    else:
        print(f"{username} is not in the match queue")
        return json.dumps({"status": "failure", "message": f"{username} is not in the match queue"})


def handleMatch(conn1, conn2, username1, username2):
    try:
        currentPlayer = username1
        board = [" " for _ in range(9)]

        conn1.settimeout(0.5)
        conn2.settimeout(0.5)

        conn1.sendall(json.dumps({"status": "success", "message": "Match found!", "board": board, "currentPlayer": username1}).encode())
        conn2.sendall(json.dumps({"status": "success", "message": "Match found!", "board": board, "currentPlayer": username1}).encode())
        print(f'Match between {username1} and {username2} has started')

        while True:
            data = None
            conn1.sendall(json.dumps({"status": "prompt_move", "message": "Your move!", "currentPlayer": currentPlayer}).encode())
            conn2.sendall(json.dumps({"status": "prompt_move", "message": "Your move!", "currentPlayer": currentPlayer}).encode())
            
            while True:
                if currentPlayer == username1:
                    try:
                        data = conn1.recv(bufferSize).decode()
                        break
                    except socket.timeout:
                        print(f'Did not receive packet from {username1}')
                    try:
                        surrenderData = conn2.recv(bufferSize).decode()
                        break
                    except socket.timeout:
                        print(f'Did not receive packet from {username2}')
                else:
                    try:
                        data = conn2.recv(bufferSize).decode()
                        break
                    except socket.timeout:
                        print(f'Did not receive packet from {username1}')
                    try:
                        surrenderData = conn1.recv(bufferSize).decode()
                        break
                    except socket.timeout:
                        print(f'Did not receive packet from {username1}')
            
            if data != None:
                print(f"Received from {currentPlayer}: {data}")
                move = int(json.loads(data)["move"])
            elif surrenderData:
                losingPlayer = username2 if currentPlayer == username1 else username1
                winningPlayer = currentPlayer
                print(f'Player {losingPlayer} is surrendering!')
                winningPlayerStats = ""
                losingPlayerStats = ""
                for player in playerStats:
                    if player['name'] == winningPlayer:
                            player['win'] += 1
                            print(f'Updated win for {winningPlayer}')
                            winningPlayerStats = f'W{player["win"]}/L{player["loss"]}/D{player["tie"]}'

                    elif player['name'] == losingPlayer:
                        player['loss'] += 1
                        print(f'Updated loss for {losingPlayer}')
                        losingPlayerStats = f'W{player["win"]}/L{player["loss"]}/D{player["tie"]}'
                
                winningMessage = "win" if winningPlayer == currentPlayer else "lose"

                conn1.sendall(json.dumps({"status": winningMessage, "winningPlayer" : winningPlayer, "losingPlayer" : losingPlayer, "winningPlayerStats": winningPlayerStats, "losingPlayerStats": losingPlayerStats}).encode())
                conn2.sendall(json.dumps({"status": "win" if winningMessage == "lose" else "lose", "winningPlayer" : winningPlayer, "losingPlayer" : losingPlayer, "winningPlayerStats": winningPlayerStats, "losingPlayerStats": losingPlayerStats}).encode())
                break
            
            if not move:
                move = 11

            if 1 <= move < 11 and board[move - 1] == " ":
                symbol = "X" if currentPlayer == username1 else "O"
                board[move - 1] = symbol
                
                conn1.sendall(json.dumps({"status": "move_success", "board": board}).encode())
                conn2.sendall(json.dumps({"status": "move_success", "board": board}).encode())
                print(f'Sent board to {username1} and {username2}')

                if checkWin(board, symbol):
                    winningPlayer = username1 if symbol == "X" else username2
                    losingPlayer = username1 if winningPlayer == username2 else username2
                    winningMessage = "win" if winningPlayer == currentPlayer else "lose"

                    winningPlayerStats = ""
                    losingPlayerStats = ""

                    for player in playerStats:
                        if player['name'] == winningPlayer:
                            player['win'] += 1
                            print(f'Updated win for {winningPlayer}')
                            winningPlayerStats = f'W{player["win"]}/L{player["loss"]}/D{player["tie"]}'

                        elif player['name'] == losingPlayer:
                            player['loss'] += 1
                            print(f'Updated loss for {losingPlayer}')
                            losingPlayerStats = f'W{player["win"]}/L{player["loss"]}/D{player["tie"]}'

                    conn1.sendall(json.dumps({"status": winningMessage, "winningPlayer" : winningPlayer, "losingPlayer" : losingPlayer, "winningPlayerStats": winningPlayerStats, "losingPlayerStats": losingPlayerStats}).encode())
                    conn2.sendall(json.dumps({"status": "win" if winningMessage == "lose" else "lose", "winningPlayer" : winningPlayer, "losingPlayer" : losingPlayer, "winningPlayerStats": winningPlayerStats, "losingPlayerStats": losingPlayerStats}).encode())

                    break

                if checkDraw(board):
                    for player in playerStats:
                        if player['name'] == username1:
                            player['tie'] += 1
                            print(f'Updated draw for {username1}')
                            player1Stats = f'W{player["win"]}/L{player["loss"]}/D{player["tie"]}'

                        elif player['name'] == username2:
                            player['tie'] += 1
                            print(f'Updated draw for {username2}')
                            player2Stats = f'W{player["win"]}/L{player["loss"]}/D{player["tie"]}'

                    conn1.sendall(json.dumps({"status": "draw", "player1" : username1, "player2" : username2, "player1Stats": player1Stats, "player2Stats": player2Stats}).encode())
                    conn2.sendall(json.dumps({"status": "draw", "player1" : username1, "player2" : username2, "player1Stats": player1Stats, "player2Stats": player2Stats}).encode())

                    break

                print(f'Old current player: {currentPlayer}')
                currentPlayer = username2 if currentPlayer == username1 else username1
                print(f'New current player: {currentPlayer}')

            elif move == 12:
                print(f'Player {json.loads(data)["surrenderingPlayer"]} is surrendering!')
                losingPlayer = json.loads(data)["surrenderingPlayer"]
                winningPlayer = username1 if username2 == losingPlayer else username2
                winningPlayerStats = ""
                losingPlayerStats = ""
                for player in playerStats:
                    if player['name'] == winningPlayer:
                            player['win'] += 1
                            print(f'Updated win for {winningPlayer}')
                            winningPlayerStats = f'W{player["win"]}/L{player["loss"]}/D{player["tie"]}'

                    elif player['name'] == losingPlayer:
                        player['loss'] += 1
                        print(f'Updated loss for {losingPlayer}')
                        losingPlayerStats = f'W{player["win"]}/L{player["loss"]}/D{player["tie"]}'
                
                winningMessage = "win" if winningPlayer == currentPlayer else "lose"

                conn1.sendall(json.dumps({"status": winningMessage, "winningPlayer" : winningPlayer, "losingPlayer" : losingPlayer, "winningPlayerStats": winningPlayerStats, "losingPlayerStats": losingPlayerStats}).encode())
                conn2.sendall(json.dumps({"status": "win" if winningMessage == "lose" else "lose", "winningPlayer" : winningPlayer, "losingPlayer" : losingPlayer, "winningPlayerStats": winningPlayerStats, "losingPlayerStats": losingPlayerStats}).encode())
                break
            elif move == 11:
                conn1.sendall(json.dumps({"status": "prompt_move", "message": "Your move!", "currentPlayer": currentPlayer}).encode())
            else:
                if currentPlayer == username1:
                    conn1.sendall(json.dumps({"status": "failure", "message": "Invalid move"}).encode())
                    print(f'Sent invalid move to {username1}')
                else:
                    conn2.sendall(json.dumps({"status": "failure", "message": "Invalid move"}).encode())
                    print(f'Sent invalid move to {username2}')

            if move != 11:
                conn1.sendall(json.dumps({"status": "success", "board": board}).encode())
                conn2.sendall(json.dumps({"status": "success", "board": board}).encode())
                print(f'Sent board move to {username1} and {username2} 2nd time.')
    finally:
        del loggedInUsers[username1]
        del loggedInUsers[username2]
        conn1.close()
        conn2.close()
        print(f"Match between {username1} and {username2} has ended and disconnected both players.")
        dumpPlayerStats()
        

def checkWin(board, player):
    winConditions = [
        [0, 1, 2], [3, 4, 5], [6, 7, 8], 
        [0, 3, 6], [1, 4, 7], [2, 5, 8], 
        [0, 4, 8], [2, 4, 6]             
    ]
    return any(all(board[pos] == player for pos in condition) for condition in winConditions)

def checkDraw(board):
    return all(space != " " for space in board)

if __name__ == "__main__":
    startServer()
